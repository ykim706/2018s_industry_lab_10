package ictgradschool.industry.recursion.ex02;

public class Reverse {

    public String reverse(String reversed){
        if(reversed.length() < 1){
            return "";
        }
        else{
            return reverse(reversed.substring(1)) + reversed.charAt(0);

        }


    }

    public static void main(String[] args) {
        System.out.println(new Reverse().reverse(""));
    }
}
